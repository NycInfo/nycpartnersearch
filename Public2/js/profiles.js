function deleteprofile(id){
  var data = {};
  data.id = id;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/deleteprofile',
    async:true,
    crossDomain:true,
      success: function(res) {
        alert("Profile Deleted");
        getallprofiles();
      }
  });
}


function getallprofiles(stts){
  var data = {};
  data.status = stts;
  $('#example1').dataTable({
     dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ],
      lengthMenu: [[100, 125, 150, -1], [110, 125, 150, "All"]],
      processing: true,
      destroy:true,
      autoWidth:false,
      jQueryUI: true,
      sAjaxDataProp: 'data',
      ajax:{
      url: "/getallprofiles",
      type: 'POST',
      data: data,
      dataSrc: ""
      },
        columns: [
          { data : "uid" },
          {
              data: "matrimony_id",
              className: "center",
              render: function(data, type, row, meta){
                      data='<td><a href="/viewpro_admin?uid='+row.uid+'">'+row.matrimony_id+'</a></td>';
                return data;
              }
        },
        {
            data: "matrimony_id",
            className: "center",
            render: function(data, type, row, meta){
                    data='<td>'+row.FirstName+' '+row.SurName+'</td>';
              return data;
            }
      },
        { data : "Password" },
        { data : "Mobile"},
        { data : "Gender"},
        { data : "MaritialStatus" },
        {
            data: "uid",
            className: "center",
            render: function(data, type, row, meta){
                    data='<td><a target="_blank" onclick=deleteprofile("'+row._id+'")>Delete</a></td>';
              return data;
            }
        },
        {
            data: "uid",
            className: "center",
            render: function(data, type, row, meta){
                    data='<td><select onchange=paymentstatus("'+row.Email+'","'+row.matrimony_id+'","'+row.uid+'","'+row._id+'",this.value)><option>Select</option>';
                    if (row.PaymentStatus == "1") {
                      data += '<option selected value="1">Paid</option><option value="0">Un Paid</option>';
                    }else if (row.PaymentStatus == "0" || row.PaymentStatus == "") {
                      data += '<option value="1">Paid</option><option selected value="0">Un Paid</option>'
                    }else {
                      data += '<option value="1">Paid</option><option value="0">Un Paid</option>';
                    }
                    data += '</select></td>';
              return data;
            }
        },
        ],
        order:[[1,"desc"]]
      });
}



function paymentstatus(email,mid,uid,id,value){
  var data = {};
  data.email = email;
  data.matrimony_id = mid;
  data.id = id;
  data.value = value;
  data.uid = uid;
  console.log(data);
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/paymentstatus',
    async:true,
    crossDomain:true,
      success: function(res) {
      if (res == "1") {
        console.log("ok");
        // alert("Payment Status Changed..");
        // location.reload();
      }else{
        alert("Some thing Wrong.. Please try later")
      }

      }
  });
}
