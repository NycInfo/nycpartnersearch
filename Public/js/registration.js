function getlatestprofiles(){
  $.ajax({
         type: 'POST',
         url: '/latestprofiles',
         crossDomain:true,
           success: function(res) {
             var msg = "";
            $.each(res,function(i,d){
             // if (d.Photo1 == undefined || d.Photo1 == "") {
                var img = "Public/images/profile_photos/noimage.jpeg"
              // }else {
              //   var img = "Public/images/profile_photos/"+d.Photo1;
              // }
              msg += '<li><div class="col_1" ><a href="/Login"><img src ='+img+' alt="" class="hover-animation image-zoom-in imgeinlatest" style="height: 174px;" /><div class="layer m_1 hidden-link hover-animation delay1 fade-in"><div class="center-middle">About Him</div></div><h3><span class="m_3">Profile ID : '+d.matrimony_id+'</span><br>Created By : '+d.ProfileCreatedBy+'<br><div style="font-size: 16px;">'+d.FirstName+' '+d.SurName+'</div></h3></a></div></li>';
            })
            $("#flexiselDemo3").html(msg);
          }
  });
}

function getsuccessstories(){
  $.ajax({
         type: 'POST',
         url: '/success_stories',
         crossDomain:true,
           success: function(res) {
             var msg = "";
            $.each(res,function(i,d){
              msg += '<li><div class="suceess_story-date"><span class="entry-1">Dec 20, 2015</span></div><div class="suceess_story-content-container"><figure class="suceess_story-content-featured-image"><img width="75" height="75" src="Public2/img/themes/'+d.BG_pic+'" class=" profileimage img-responsive" alt=""/></figure><div class="suceess_story-content-info"><h4><a href="#">'+d.Bride_Name+' & '+d.Groom_Name+'</a></h4><p>'+d.Description+'</p></div></div></li>';
            })
            $("#Successstories").html(msg);
          }
  });
}
function getevents(){
  $.ajax({
         type: 'POST',
         url: '/eventfunctions',
         crossDomain:true,
           success: function(res) {
             var msg = "";
            $.each(res,function(i,d){
              //console.log(d.event_pic);
              if (d.event_pic == "") {
                var img = "Public/images/Services/events.png";
              }else {
                var img = "Public2/img/themes/"+d.event_pic;
              }
              console.log(img);
              var date = moment(d.dateofevent).format("DD");
              var month = moment(d.dateofevent).format("LL");
              msg += '<div class="box_1"><figure class="thumbnail1"><img width="170" height="155" src='+img+' class="img-responsive" alt=""/></figure><div class="extra-wrap"><div class="post-meta"><span class="day"><time datetime="2014-05-25T10:15:43+00:00">'+date+'</time></span><span class="month"><time datetime="2014-05-25T10:11:51+00:00">'+month+'</time></span></div><h4 class="post-title"><a href="#">'+d.eventname+'</a></h4><div class="clearfix"> </div><div class="post-content">'+d.eventdescr+'</div><a href="#" class="vertical">'+d.street+', '+d.city+', Land Mark: '+d.landmark+'</a></div></div>';
            })
            $("#events").html(msg);
          }
  });
}


$("#login_button").click(function(){
  var data = {};
  data.username = $("#username").val().toUpperCase();
  if (data.username == '') {
      $('#username').focus().notify("Please Enter Username", { className: "error", position: "bottom" });
      return false;
  }
  data.password = $("#password").val();
  if (data.password == '') {
      $('#password').focus().notify("Please Enter Password", { className: "error", position: "bottom" });
      return false;
  }
  // $("#login_button").prop('disabled', true);
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/Profilelogin',
    async:true,
    crossDomain:true,
      success: function(res) {
        if(res == "1"){
          window.location="/home"
          return false;
        }else{
          $('#login_button').focus().notify("Username/Password Invalid", { className: "error", position: "bottom" });
          return false;
        }
      }
  });
  return false;
})


$("#Registration_one").click(function(){
  var data = {};
  data.profilefor = $("#profilefor").val();
  if (data.profilefor == '') {
      $('#profilefor').focus().notify("Please Select ..", { className: "error", position: "bottom" });
      return false;
  }
  data.firstname = $("#firstname").val();
  if (!checkInputAlpha($('#firstname').val().trim()) || $('#firstname').val().trim() == '') {
      $('#firstname').focus().notify("Please Enter Valid First Name.", { className: "error", position: "bottom" });
      return false;
  }
  data.lastname = $("#lastname").val();
  if (!checkInputAlpha($('#lastname').val().trim()) || $('#lastname').val().trim() == '') {
      $('#lastname').focus().notify("Please Enter Valid Last Name.", { className: "error", position: "bottom" });
      return false;
  }
  data.gender = $("#gender").val();
  if (data.gender == '') {
      $('#gender').focus().notify("Please Select ..", { className: "error", position: "bottom" });
      return false;
  }
  data.email = $("#email").val();
  // if(!checkInputEmail($('#email').val().trim())){
  //     $('#email').focus().notify("Please Enter Valid Email...", { className: "error", position:"bottom" });
  //     return false;
  // }
  data.password = $("#password").val();
  if (data.password == '') {
      $('#password').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
      return false;
  }
  data.mobilenumber = $("#mobilenumber").val();
  if(!checkInputPhNum($('#mobilenumber').val())){
      $('#mobilenumber').focus().notify("Please Enter Mobile No...", { className: "error", position:"bottom" });
      return false;
  }
  data.dob_date = $("#dob_date").val();
  if (data.dob_date == '') {
      $('#dob_date').focus().notify("Please Enter ..", { className: "error", position: "bottom" });
      return false;
  }
  data.dob_month = $("#dob_month").val();
  if (data.dob_month == '') {
      $('#dob_month').focus().notify("Please Enter ..", { className: "error", position: "bottom" });
      return false;
  }
  data.dob_year = $("#dob_year").val();
  if (data.dob_year == '') {
      $('#dob_year').focus().notify("Please Enter DOB ..", { className: "error", position: "bottom" });
      return false;
  }
  data.maritalstatus = $("#maritalstatus").val();
  if (data.maritalstatus == '') {
      $('#maritalstatus').focus().notify("Please Enter Maritial Status ..", { className: "error", position: "bottom" });
      return false;
  }
  
  data.mothertongue = $("#mothertongue").val();
  if (data.mothertongue == '') {
      $('#mothertongue').focus().notify("Please Enter Mother Tongue ..", { className: "error", position: "bottom" });
      return false;
  }
  data.country = $("#country").val();
  if (data.country == '') {
      $('#country').focus().notify("Please Enter Country ..", { className: "error", position: "bottom" });
      return false;
  }
  $("#registersteptwo").show();
  $("#thirdregistration").hide();
  $("#registerstepone").hide();
  $("#timeofbirth").focus();
  return false;
})

$("#Registration_two").click(function(){
  var data = {};
  data.profilefor = $("#profilefor").val();
  data.firstname = $("#firstname").val();
  data.lastname = $("#lastname").val();
  data.gender = $("#gender").val();
  data.email = $("#email").val();
  data.password = $("#password").val();
  data.mobilenumber = $("#mobilenumber").val();
  data.dob_date = $("#dob_date").val();
  data.dob_month = $("#dob_month").val();
  data.dob_year = $("#dob_year").val();
  data.maritalstatus = $("#maritalstatus").val();
  data.religion = $("#religion").val();
  data.caste = $("#caste").val();
  data.subcaste = $("#subcaste").val();
  data.mothertongue = $("#mothertongue").val();
  data.country = $("#country").val();


  data.timeofbirth = $("#timeofbirth").val();
  data.placeofbirth = $("#placeofbirth").val();
  data.star = $("#star").val();
  data.padam = $("#padam").val();
  data.rasi = $("#rasi").val();
  data.gothram = $("#gothram").val();
  // if (data.gothram == '') {
  //     $('#gothram').focus().notify("Please Enter Gothram ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.complexian = $("#complexian").val();
  if (data.complexian == '') {
      $('#complexian').focus().notify("Please Enter Complexian ..", { className: "error", position: "bottom" });
      return false;
  }
  data.height = $("#height").val();
  if (data.height == '') {
      $('#height').focus().notify("Please Enter height ..", { className: "error", position: "bottom" });
      return false;
  }
  data.weight = $("#weight").val();
  // if (data.complexian == '') {
  //     $('#complexian').focus().notify("Please Enter weight ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.Education = $("#Education").val();
  if (data.Education == '') {
      $('#Education').focus().notify("Please Enter Education ..", { className: "error", position: "bottom" });
      return false;
  }
  data.Specialiation = "";
  
  data.Occupation = $("#Occupation").val();
  // if (data.Occupation == '') {
  //     $('#Occupation').focus().notify("Please Enter Occupation ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.Working = $("#Working").val();
  // if (data.Working == '') {
  //     $('#Working').focus().notify("Please Enter Working ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.Salary = $("#Salary").val();
  // if (data.Salary == '') {
  //     $('#Salary').focus().notify("Please Enter Salary ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.ancestralorigin = $("#ancestralorigin").val();
  data.kujadosam = $("#kujadosam").val();
  // if (data.kujadosam == '') {
  //     $('#kujadosam').focus().notify("Please Enter kujadosam ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.fathername = $("#fathername").val();
  // if (data.fathername == '') {
  //     $('#fathername').focus().notify("Please Enter fathername ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.fatheroccupation = $("#fatheroccupation").val();
  // if (data.fatheroccupation == '') {
  //     $('#fatheroccupation').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.mothername = $("#mothername").val();
  // if (data.mothername == '') {
  //     $('#mothername').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.motheroccupation = $("#motheroccupation").val();
  // if (data.motheroccupation == '') {
  //     $('#motheroccupation').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.gaurdianname = "";

  data.gaurdianoccupation = "";
  data.brothers = $("#brothers_reg").val();

  data.sisters = $("#sisters_reg").val();
  data.marriedbrothers = $("#marriedbrothers").val();
  data.marriedsisters = $("#marriedsisters").val();
  data.Address = $("#Address_reg").val();
  data.Village = $("#Village").val();
  // if (data.Village == '') {
  //     $('#Village').focus().notify("Please Enter Village ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.City = $("#City").val();
  // if (data.City == '') {
  //     $('#City').focus().notify("Please Enter City ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.State = $("#State").val();
  // if (data.State == '') {
  //     $('#State').focus().notify("Please Enter State ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.Country = $("#Country").val();
  if (data.Country == '') {
      $('#Country').focus().notify("Please Enter Country ..", { className: "error", position: "bottom" });
      return false;
  }
  data.Reference = $("#Reference").val();
  data.alternatemobile = $("#alternatemobile").val();
  data.familydetails = $("#familydetails").val();
  data.propertydetails = $("#propertydetails").val();
  $("#registersteptwo").hide();
  $("#registerstepone").hide();
  $("#thirdregistration").show();
  return false;
})


$("#profilesubbtnpartners").click(function(){
  var data = {};
  data.profilefor = $("#profilefor").val();
  data.firstname = $("#firstname").val();
  data.lastname = $("#lastname").val();
  data.gender = $("#gender").val();
  data.email = $("#email").val();
  data.emailnotification =  $("input[name='notifyemail']:checked").val();

  data.password = $("#password").val();
  data.mobilenumber = $("#mobilenumber").val();
  data.dob_date = $("#dob_date").val();
  data.dob_month = $("#dob_month").val();
  data.dob_year = $("#dob_year").val();
  data.maritalstatus = $("#maritalstatus").val();
  data.religion = $("#religion").val();
  data.caste = $("#caste").val()
  data.subcaste = $("#subcaste").val();
  data.mothertongue = $("#mothertongue").val();
  data.country = $("#country").val();
  data.timeofbirth = $("#timeofbirth").val();
  data.placeofbirth = $("#placeofbirth").val();
  data.star = $("#star").val();
  data.padam = $("#padam").val();
  data.rasi = $("#rasi").val();
  data.gothram = $("#gothram").val();

  data.complexian = $("#complexian").val();

  data.height = $("#height").val();
 
  data.weight = $("#weight").val();

  data.Education = $("#Education").val();
 
  data.Occupation = $("#Occupation").val();

  data.Working = $("#Working").val();
 
  data.Salary = $("#Salary").val();
 
  data.ancestralorigin = $("#ancestralorigin").val();
  data.kujadosam = $("#kujadosam").val();

  data.fathername = $("#fathername").val();

  data.fatheroccupation = $("#fatheroccupation").val();

  data.mothername = $("#mothername").val();

  data.motheroccupation = $("#motheroccupation").val();

  data.brothers = $("#brothers_reg").val();
  data.sisters = $("#sisters_reg").val();
  data.marriedbrothers = $("#marriedbrothers").val();
  data.marriedsisters = $("#marriedsisters").val();
  data.Address = $("#Address_reg").val();
  data.Village = $("#Village").val();
  data.City = $("#City").val();
  
  data.State = $("#State").val();
  
  data.Country = $("#Country").val();
  
  data.Reference = $("#Reference").val();
  data.alternatemobile = $("#alternatemobile").val();
  data.familydetails = $("#familydetails").val();
  data.propertydetails = $("#propertydetails").val();

  data.P_AboutPartner = $("#About_Partner").val();
  
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/Registration',
    async:true,
    crossDomain:true,
      success: function(res) {
        if(res == 1){
          $('#profilesubbtnpartners').focus().notify("Successfully Updated..", { className: "success", position: "bottom" });
          setTimeout(function(){ window.location='/Login' }, 2000);
          return false;
        }else{
          $('#profilesubbtnpartners').focus().notify("Network Error..PLease Try Later..", { className: "error", position: "bottom" });
          return false;
        }
      }
  });
  return false;
})

$(".mobilenumber_reg").change(function(){
  var data = {};
  data.phoneentered = $("#mobilenumber").val();
  // data.emailentered = $("#email").val();
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/checkingemailisregisteredalready',
      async:true,
      crossDomain:true,
        success: function(res) {
         if(res >= "1"){
           $('#mobilenumber').focus().notify("Phone Number Already Exist", { className: "error", position:"bottom" });
           $("#mobilenumber").val("");
           return false;
         }else{

         }
        }
    });
})


$("#EditRegistration_one").click(function(){
  var data = {};
  data.profilefor = $("#profilefor").val();
  if (data.profilefor == '') {
      $('#profilefor').focus().notify("Please Select ..", { className: "error", position: "bottom" });
      return false;
  }
  
  data.firstname = $(".efirstname").val();
  
  if ($('.efirstname').val() == '') {
      $('.efirstname').focus().notify("Please Enter Valid First Name.", { className: "error", position: "bottom" });
      return false;
  }
  
  data.lastname = $("#lastname").val();
  if (!checkInputAlpha($('#lastname').val().trim()) || $('#lastname').val().trim() == '') {
      $('#lastname').focus().notify("Please Enter Valid Last Name.", { className: "error", position: "bottom" });
      return false;
  }
  
  data.gender = $("#gender").val();
  if (data.gender == '') {
      $('#gender').focus().notify("Please Select ..", { className: "error", position: "bottom" });
      return false;
  }
  
  data.email = $("#email").val();
  if(!checkInputEmail($('#email').val().trim())){
      $('#email').focus().notify("Please Enter Valid Email...", { className: "error", position:"bottom" });
      return false;
  }
  data.password = $("#password").val();
  if (data.password == '') {
      $('#password').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
      return false;
  }
  
  data.mobilenumber = $("#mobilenumbermm").val();
  if(!checkInputPhNum($('#mobilenumbermm').val())){
      $('#mobilenumbermm').focus().notify("Please Enter Mobile No...", { className: "error", position:"bottom" });
      return false;
  }
  data.dob_date = $("#dob_date").val();
  if (data.dob_date == '') {
      $('#dob_date').focus().notify("Please Enter ..", { className: "error", position: "bottom" });
      return false;
  }
  data.dob_month = $("#dob_month").val();
  if (data.dob_month == '') {
      $('#dob_month').focus().notify("Please Enter ..", { className: "error", position: "bottom" });
      return false;
  }
  data.dob_year = $("#dob_year").val();
  if (data.dob_year == '') {
      $('#dob_year').focus().notify("Please Enter DOB ..", { className: "error", position: "bottom" });
      return false;
  }
  data.maritalstatus = $("#maritalstatus").val();
  if (data.maritalstatus == '') {
      $('#maritalstatus').focus().notify("Please Enter Maritial Status ..", { className: "error", position: "bottom" });
      return false;
  }
  data.caste = $("#castee").val();
  data.religion = $("#religione").val();

  data.mothertongue = $("#mothertongue").val();
  if (data.mothertongue == '') {
      $('#mothertongue').focus().notify("Please Enter Mother Tongue ..", { className: "error", position: "bottom" });
      return false;
  }
  data.country = $("#country").val();
  if (data.country == '') {
      $('#country').focus().notify("Please Enter Country ..", { className: "error", position: "bottom" });
      return false;
  }
  $("#registerstepone").hide();
  $("#registersteptwo").show();
})

$("#EditRegistration_two").click(function(){
  var data = {};
  data.profilefor = $("#profilefor").val();
  
  data.firstname = $(".efirstname").val();
  
  data.lastname = $("#lastname").val();
  
  data.gender = $("#gender").val();
  
  data.email = $("#email").val();
  
  data.password = $("#password").val();
  
  data.mobilenumber = $("#mobilenumbermm").val();
  
  data.dob_date = $("#dob_date").val();
  
  data.dob_month = $("#dob_month").val();
  
  data.dob_year = $("#dob_year").val();
  
  data.maritalstatus = $("#maritalstatus").val();
  
  data.caste = $("#castee").val();
  data.religion = $("#religione").val();

  data.mothertongue = $("#mothertongue").val();


  data.timeofbirth = $("#timeofbirth").val();
  data.star = $("#star").val();
  data.padam = $("#padam").val();
  data.rasi = $("#rasi").val();
  data.gothram = $("#gothram").val();
  data.complexian = $("#complexian").val();
  if (data.complexian == '') {
      $('#complexian').focus().notify("Please Enter Complexian ..", { className: "error", position: "bottom" });
      return false;
  }
  data.height = $("#height").val();
  if (data.height == '') {
      $('#height').focus().notify("Please Enter height ..", { className: "error", position: "bottom" });
      return false;
  }
  data.weight = $("#weight").val();
  // if (data.complexian == '') {
  //     $('#complexian').focus().notify("Please Enter weight ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.Education = $("#Education").val();
  if (data.Education == '') {
      $('#Education').focus().notify("Please Enter Education ..", { className: "error", position: "bottom" });
      return false;
  }
  data.Specialiation = $("#Specialiation").val();
  
  data.Occupation = $("#Occupation").val();
  // if (data.Occupation == '') {
  //     $('#Occupation').focus().notify("Please Enter Occupation ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.Working = $("#Working").val();
  // if (data.Working == '') {
  //     $('#Working').focus().notify("Please Enter Working ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.Salary = $("#Salary").val();
  if (data.Salary == '') {
      $('#Salary').focus().notify("Please Enter Salary ..", { className: "error", position: "bottom" });
      return false;
  }
  data.ancestralorigin = $("#ancestralorigin").val();
  data.kujadosam = $("#kujadosam").val();
  // if (data.kujadosam == '') {
  //     $('#kujadosam').focus().notify("Please Enter kujadosam ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.fathername = $("#fathername").val();
  if (data.fathername == '') {
      $('#fathername').focus().notify("Please Enter fathername ..", { className: "error", position: "bottom" });
      return false;
  }
  data.fatheroccupation = $("#fatheroccupation").val();
  // if (data.fatheroccupation == '') {
  //     $('#fatheroccupation').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.mothername = $("#mothername").val();
  if (data.mothername == '') {
      $('#mothername').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
      return false;
  }
  data.motheroccupation = $("#motheroccupation").val();
  // if (data.motheroccupation == '') {
  //     $('#motheroccupation').focus().notify("Please Enter Password ..", { className: "error", position: "bottom" });
  //     return false;
  // }
  data.gaurdianname = "";

  data.gaurdianoccupation = "";
  data.brothers = $("#brothers_reg").val();

  data.sisters = $("#sisters_reg").val();
  data.marriedbrothers = $("#marriedbrothers").val();
  data.marriedsisters = $("#marriedsisters").val();
  data.Address = $("#Address_reg").val();
  data.Village = $("#Village").val();
  data.City = $("#City").val();
  if (data.City == '') {
      $('#City').focus().notify("Please Enter City ..", { className: "error", position: "bottom" });
      return false;
  }
  data.State = $("#State").val();
  if (data.State == '') {
      $('#State').focus().notify("Please Enter State ..", { className: "error", position: "bottom" });
      return false;
  }
  data.Reference = $("#Reference").val();
  data.alternatemobile = $("#alternatemobile").val();
  data.familydetails = $("#familydetails").val();
  data.propertydetails = $("#propertydetails").val();
  data.partnerpreference = $("#partnerpreference").val();
  data.source = "user";
  data.country = $(".countrye").val();

  console.log(data)
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/EditProfile',
    async:true,
    crossDomain:true,
      success: function(res) {
        if(res == 1){
          $('#EditRegistration_two').focus().notify("Successfully Updated..", { className: "success", position: "bottom" });
          setTimeout(function(){ window.location='/My_Profile' }, 2000);
          
          return false;
        }else{
          $('#EditRegistration_two').focus().notify("Network Error..PLease Try Later..", { className: "error", position: "bottom" });
          return false;
        }
      }
  });
})

