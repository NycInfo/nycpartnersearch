function viewprofile(uid){
  var data = {};
  data.uid = uid;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getprofiledetailsfromusers',
    async:true,
    crossDomain:true,
      success: function(res) {
        console.log(res)
        $("#rowid").val(res._id)
        if (res.Photo1 == undefined || res.Photo1 == "") {
          var img1 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img1 = "Public/images/profile_photos/"+res.Photo1;
        }
        if (res.Photo2 == undefined || res.Photo2 == "") {
          var img2 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img2 = "Public/images/profile_photos/"+res.Photo2;
        }
        if (res.Photo3 == undefined || res.Photo3 == "") {
          var img3 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img3 = "Public/images/profile_photos/"+res.Photo3;
        }
        if (res.Photo4 == undefined || res.Photo4 == "") {
          var img4 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img4 = "Public/images/profile_photos/"+res.Photo4;
        }
        var viewimages = '<li data-thumb="'+img1+'"><img src="'+img1+'" /></li><li data-thumb="'+img2+'"><img src="'+img2+'" /></li><li data-thumb="'+img3+'"><img src="'+img3+'" /></li><li data-thumb="'+img4+'"><img src="'+img4+'" /></li>';
        $("#viewprofile_images").html(viewimages);
        $('.flexslider').flexslider({
          animation: "slide",
          controlNav: "thumbnails"
        });
        var age = calculateage(res.Dob)
        $("#mat_id").html(res.matrimony_id);
        $(".age").html(age);
        $("#mem_email").html(res.Email);
        $("#mobile").html(res.Mobile);
        $("#created_by").html(res.ProfileCreatedBy);
        $(".username").html(res.FirstName+' '+res.SurName);
        $("#marital_status").html(res.MaritialStatus);
        $("#religion").html(res.Religion);
        $("#religionp").html(res.Religion);

        
        $("#caste").html(res.Caste);
        $("#mother_tongue").html(res.MotherTongue);
        $("#profile_name").html(res.FirstName+' '+res.SurName);
        $("#profile_mstatus").html(res.MaritialStatus);
        $("#profile_createdfor").html(res.ProfileCreatedBy);
        $("#profile_age").html(age);
        $("#profile_mothertogue").html(res.MotherTongue);
        $("#profile_dob").html(moment(res.Dob).format("YYYY-MM-DD"));
        $("#views").html(res.views)


        $("#Height").html(res.Height);
        $("#profile_tob").html(res.Timeofbirth);
        $("#profile_pob").html(res.Placeofbirth);
        $("#profile_raasi").html(res.Rasi);
        $("#profile_star").html(res.Star);
        $("#profile_padam").html(res.Padam);
        $("#profile_gothram").html(res.Gothram);
        $("#mem_complexion").html(res.Complexion);
        $("#mem_weight").html(res.Weight);


        $("#mem_education").html(res.Education);
        $("#mem_occupation").html(res.Occupation);
        $("#mem_working").html(res.WorkingIn);
        $("#mem_salary").html(res.Salary);
        //Fmily Details
        $("#father_name").html(res.FatherName);
        $("#father_occuption").html(res.FatherOccupation);
        $("#mother_name").html(res.MotherName);
        $("#mother_occuption").html(res.MotherOccupation);
       
        $("#brothers").html(res.Brothers);
        $("#married_brothers").html(res.MarriedBrothers);
        $("#sisters").html(res.Sisters);
        $("#married_sisters").html(res.MarriedSisters);



        $("#mem_ancestralorigin").html(res.Origin);

        $("#mem_kujadosham").html(res.Kujadosam);

        $("#partner_preferences").html(res.P_AboutPartner);

        $("#address").html(res.Address +"--"+res.City);
        $("#alternate_email").html(res.Email);
        $("#alternate_mobile").html(res.Mobile);

        $("#family_details").html(res.FamilyDetails);
        $("#property_details").html(res.PropertyDetails);

        $("#profile_complexian").html(res.Complexion);
        $("#profile_height").html(res.Height);

        $("#address").html(res.Address);
        $("#address_ad").html(res.Address);
        $("#city_ad").html(res.City);
        $("#view_partner_age").html(res.P_PartnerAgeFrom+" - "+res.P_PartnerAgeTo);
        $("#view_partnerheight").html(res.P_PartnerHeightFrom+" - "+res.P_PartnerHeightFromTo);
        $("#view_partnercomplexion").html(res.P_PartnerComplexion);
        $("#view_partnermaritialstatus").html(res.P_PartnerMaritalStatus);
        $("#view_partner_children").html(res.P_PartnerHaveChildren);
        $("#view_partnerdosam").html(res.P_PartnerDosam);
        $("#view_partnermothertongue").html(res.P_PartnerMotherTongue);
        $("#view_partnerfamilyvalue").html(res.P_PartnerFamilyValue);
        $("#view_partneremployedas").html(res.P_P_PartnerEmployedAs);
        $("#view_partnerannualincome").html(res.P_PartnerAnnuIncome);
        $("#view_partnerpropertyrange").html(res.P_PartnerPropertyRange);
        $("#view_partnerfamilyproperty").html(res.P_FamilyProperty);
        $("#view_partnerfamilypropertydetails").html(res.P_FamilyPropertyDetails);
        $("#view_partneraboutpartner").html(res.P_AboutPartner);
      }
  });


}


function myprofile(uid,source){
  var data = {};
  data.uid = uid;
  data.source=source;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getmyprofiledetailsfromusers',
    async:true,
    crossDomain:true,
      success: function(res) {
        $("#photo1manage").val(res.Photo1)
        $("#photo2manage").val(res.Photo2)

        $("#photo3manage").val(res.Photo3)
        $("#photo4manage").val(res.Photo4)

        if (res.Photo1 == undefined || res.Photo1 == "") {
          var img1 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img1 = "Public/images/profile_photos/"+res.Photo1;
        }
        if (res.Photo2 == undefined || res.Photo2 == "") {
          var img2 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img2 = "Public/images/profile_photos/"+res.Photo2;
        }
        if (res.Photo3 == undefined || res.Photo3 == "") {
          var img3 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img3 = "Public/images/profile_photos/"+res.Photo3;
        }
        if (res.Photo4 == undefined || res.Photo4 == "") {
          var img4 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img4 = "Public/images/profile_photos/"+res.Photo4;
        }
        var viewimages = '<li data-thumb="'+img1+'"><img src="'+img1+'" /></li><li data-thumb="'+img2+'"><img src="'+img2+'" /></li><li data-thumb="'+img3+'"><img src="'+img3+'" /></li><li data-thumb="'+img4+'"><img src="'+img4+'" /></li>';
        $("#myprofile_images").html(viewimages);
        $('.flexslider').flexslider({
          animation: "slide",
          controlNav: "thumbnails"
        });
        var image1 = '<img src="'+img1+'"  class="img-responsive" width="100%" alt="">';
        var image2 = '<img src="'+img2+'"  class="img-responsive" width="100%" alt="">';
        var image3 = '<img src="'+img3+'"  class="img-responsive" width="100%" alt="">';
        var image4 = '<img src="'+img4+'"  class="img-responsive" width="100%" alt="">';
        $("#photo1").html(image1);
        $("#photo2").html(image2);
        $("#photo3").html(image3);
        $("#photo4").html(image4);
        var age = calculateage(res.Dob)
        $("#mat_id").html(res.matrimony_id);
        $(".age").html(age);
        $("#mem_email").html(res.Email);
        $("#mobile").html(res.Mobile);
        $("#viewstotal").html(res.views)
        $("#created_by").html(res.ProfileCreatedBy);
        console.log(res.ProfileCreatedBy)
        $("#profilefor").val(res.ProfileCreatedBy);

        $(".username").html(res.FirstName+' '+res.SurName);
        $("#marital_status").html(res.MaritialStatus);
        $("#caste").html(res.Caste);
        $("#castee").val(res.Caste);
        
        $("#religion").html(res.Religion);
        $("#profile_religion").html(res.Religion);
        $("#mother_tongue").html(res.MotherTongue);

        $("#profile_name").html(res.FirstName+' '+res.SurName);
        $("#profile_mstatus").html(res.MaritialStatus);
        $("#profile_createdfor").html(res.ProfileCreatedBy);
        $("#profile_age").html(age);
        $("#profile_mothertogue").html(res.MotherTongue);
        $("#profile_dob").html(moment(res.Dob).format("YYYY-MM-DD"));

        $(".efirstname").val(res.FirstName);
        $("#lastname").val(res.SurName);
        $("#gender").val(res.Gender);
        $("#email").val(res.Email);
        $("#password").val(res.Password);
        $("#mobilenumber").val(res.Mobile);
        $(".mobilenumbermm").val(res.Mobile);
        var date = moment(res.Dob).format("DD")
        var month = moment(res.Dob).format("MM")
        var year = moment(res.Dob).format("YYYY")
        $(".dob_date").val(date);
        $(".dob_month").val(month);
        $(".dob_year").val(year);
        $("#maritalstatus").val(res.MaritialStatus);
        $("#religione").val(res.Religion);
        $("#mothertongue").val(res.MotherTongue);
        $("#country").val(res.Country);
        console.log(res.Country)
        $(".countrye").val(res.Country)

        $("#Height").html(res.Height);
        $("#profile_tob").html(res.Timeofbirth);
        $("#profile_pob").html(res.Placeofbirth);
        $("#profile_raasi").html(res.Star);
        $("#profile_gothram").html(res.Gothram);
        $("#mem_complexion").html(res.Complexion);
        $("#mem_weight").html(res.Weight);

        //Education Details
       
          var education = res.Education;
        
        $("#mem_education").html(education);
        
        $("#mem_occupation").html(res.Occupation);
        $("#mem_working").html(res.WorkingIn);
        $("#mem_salary").html(res.Salary);
        //Fmily Details
        $("#father_name").html(res.FatherName);
        $("#father_occuption").html(res.FatherOccupation);
        $("#mother_name").html(res.MotherName);
        $("#mother_occuption").html(res.MotherOccupation);
       
        $("#brothers").html(res.Brothers);
        $("#married_brothers").html(res.MarriedBrothers);
        $("#sisters").html(res.Sisters);
        $("#married_sisters").html(res.MarriedSisters);



        $("#mem_ancestralorigin").html(res.Origin);
       

        $("#mem_kujadosham").html(res.Kujadosam);
        //Preferences
        $("#partner_age").html(res.P_PartnerAgeFrom+" - "+res.P_PartnerAgeTo);
        $("#partnerheight").html(res.P_PartnerHeightFrom+" - "+res.P_PartnerHeightFromTo);
        $("#partnercomplexion").html(res.P_PartnerComplexion);
        $("#partnermaritialstatus").html(res.P_PartnerMaritalStatus);
        $("#partner_children").html(res.P_PartnerHaveChildren);
        $("#partnerdosam").html(res.P_PartnerDosam);
        $("#partnermothertongue").html(res.P_PartnerMotherTongue);
        $("#partnerfamilyvalue").html(res.P_PartnerFamilyValue);
        $("#partneremployedas").html(res.P_P_PartnerEmployedAs);
        $("#partnerannualincome").html(res.P_PartnerAnnuIncome);
        $("#partnerpropertyrange").html(res.P_PartnerPropertyRange);
        $("#partnerfamilyproperty").html(res.P_FamilyProperty);
        $("#partnerfamilypropertydetails").html(res.P_FamilyPropertyDetails);
        $("#partneraboutpartner").html(res.P_AboutPartner);
        $("#partnerpreference").val(res.P_AboutPartner);

        

        $("#partner_preferences").html(res.partner_preferences);
        $("#address").html(res.Address);
        $("#alternate_mobile").html(res.AlternameMobile);
        $("#family_details").html(res.FamilyDetails);
        $("#property_details").html(res.PropertyDetails);

        $("#profile_complexian").html(res.Complexion);
        $("#profile_height").html(res.Height);

        $("#timeofbirth").val(res.Timeofbirth);
        $("#star").val(res.Star);
        $("#padam").val(res.Padam);

          $("#rasi").val(res.Rasi);
          $("#profile_raasi").html(res.Rasi);
          $("#profile_star").html(res.Star);
          $("#profile_padam").html(res.Padam);
          $("#gothram").val(res.Padam);

          $("#complexian").val(res.Complexion);
          $("#height").val(res.Height);

          $("#weight").val(res.Weight);
          $("#Education").val(res.Education);

          $("#Occupation").val(res.Occupation);
          $("#Working").val(res.WorkingIn);
          $("#Salary").val(res.Salary);
          $("#kujadosam").val(res.Kujadosam);
          $("#fathername").val(res.FatherName);
          $("#fatheroccupation").val(res.FatherOccupation)
          $("#mothername").val(res.MotherName);
          $("#motheroccupation").val(res.MotherOccupation);
          $("#brothers_reg").val(res.Brothers);
          $("#sisters_reg").val(res.Sisters);
          $("#marriedbrothers").val(res.MarriedBrothers);
          $("#marriedsisters").val(res.MarriedSisters);
          $("#Address_reg").val(res.Address);
          $("#Village").val(res.Village);
          $("#City").val(res.City);
          $("#State").val(res.State);
          $("#Country").val(res.Country);
          $("#Reference").val(res.Reference);
          $("#alternatemobile").val(res.AlternameMobile);
          $("#familydetails").val(res.FamilyDetails);
          $("#propertydetails").val(res.PropertyDetails);

      }
  });

}

function getsimilarprofiles(){
  var data = {};
  data.page = 1;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getsimilarprofiles',
    async:true,
    crossDomain:true,
      success: function(res) {
        var msg = '';
        if(res.length>0){
          $.each(res,function(i,d){
            if (d.Photo1 == undefined || d.Photo1 == "") {
              var img1 = "Public/images/profile_photos/noimage.jpeg"
            }else {
              var img1 = "Public/images/profile_photos/"+d.Photo1;
            }
            msg += '<ul class="profile_item"><a href="/viewprofile?uid='+d.uid+'"><li class="profile_item-img"><img src="'+img1+'" class="img-responsive" alt=""/></li><li class="profile_item-desc"><h4>'+d.matrimony_id+'</h4><p>'+d.FirstName+' '+d.SurName+'</p><h5><a href="/viewprofile?uid='+d.uid+'">View Full Profile</a></h5></li><div class="clearfix"> </div></a></ul>';
          })
          $("#similiarprofiles").html(msg);
        }else{
          msg += "No Records Found.."
        }
      }
  });
}


function calculateage(dob){
  var dateTime = moment(dob).format("YYYY-MM-DD");
  var years = moment().diff(dateTime, 'years',false);
  return years;
}
