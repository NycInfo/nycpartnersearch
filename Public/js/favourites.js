
function getfavouriteprofiles(status) {
  var data = {};
  data.page = 1;
  data.Status = status;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getfourites',
    async: true,
    crossDomain: true,
    success: function (res) {
      console.log(res);
      var msg = '';
      if (res.length > 0) {
        $.each(res, function (i, d) {
          console.log(d);
          if (status == "3") {
            msg += '<div class=""><a target="/blank" href="/viewprofile?uid=' + d.Fromuid + '"><h2>' + d.FromMatrimonyId + '</h2>';
            msg += '<div class="col-sm-3">';
            msg += '<ul class="login_details1">';
            msg += '<li><a target="/blank" href="/viewprofile?uid=' + d.Fromuid + '">' + d.FromName + '</a></li>'
            msg += '<li><p></p></li></ul>'
            msg += '</div>'
            msg += '<div class="col-sm-6">'
            msg += '<table class="table_working_hours">'
            msg += '<tbody>'
            // msg += '<tr class="opened"><td >Age :</td><td class="day_value">'+calculateage(d.Dob)+'</td></tr>'
            msg += '</tbody>'
            msg += '</table>';
            // msg += '<div class="buttons"><a><div class="vertical">Show Contact</div></a>';
            //msg += '<a onclick=removefavs("' + d._id + '")><div class="vertical">Remove From Favourtes</div></a>'
            msg += '<a href="/viewprofile?uid=' + d.Fromuid + '"><div class="vertical">View Details</div></a>'
            msg += '</div></div><div class="clearfix"> </div></a></div><br>';
          } else {
            msg += '<div class=""><a target="/blank" href="/viewprofile?uid=' + d.Touid + '"><h2>' + d.ToMatrimonyId + '</h2>';
            msg += '<div class="col-sm-3">';
            msg += '<ul class="login_details1">';
            msg += '<li><a target="/blank" href="/viewprofile?uid=' + d.uid + '">' + d.ToName + '</a></li>'
            msg += '<li><p></p></li></ul>'
            msg += '</div>'
            msg += '<div class="col-sm-6">'
            msg += '<table class="table_working_hours">'
            msg += '<tbody>'
            // msg += '<tr class="opened"><td >Age :</td><td class="day_value">'+calculateage(d.Dob)+'</td></tr>'
            msg += '</tbody>'
            msg += '</table>';
            // msg += '<div class="buttons"><a><div class="vertical">Show Contact</div></a>';
            msg += '<a onclick=removefavs("' + d._id + '")><div class="vertical">Remove From Favourtes</div></a>'
            msg += '<a href="/viewprofile?uid=' + d.Touid + '"><div class="vertical">View Details</div></a>'
            msg += '</div></div><div class="clearfix"> </div></a></div><br>';
          }

        })
        $("#newmatches").html(msg);
      } else {
        msg += "No Records Found.."
      }
    }
  });
}

function removefavs(id) {
  var data = {};
  data.id = id;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/removefourites',
    async: true,
    crossDomain: true,
    success: function (res) {
      if (res == "1") {
        alert("Removed from favourite list");
        location.reload();
      } else {
        alert("Some thing went wrong.. Please try later");
      }
    }
  });
  return false;
}