function getprofiles(page){
  var data = {};
  data.pagination = page;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getprofiles',
    async:true,
    crossDomain:true,
      success: function(res) {
        var msg = '';
        if(res.length>0){
          $.each(res,function(i,d){
            msg += '<div class=""><a href="/viewprofile?uid='+d.uid+'"><h2>'+d.matrimony_id+'</h2>';
         	  msg += '<div class="col-sm-3 profile_left-top">';
            if (d.Photo1 == undefined || d.Photo1 == "") {
              var img = "Public/images/profile_photos/noimage.jpeg"
            }else {
              var img = "Public/images/profile_photos/"+d.Photo1;
            }
            msg += '<a href="/viewprofile?uid='+d.uid+'"><img src="'+img+'" class="img-responsive profileimage" alt=""/></a>';
            msg += '</div>'
            msg += '<div class="col-sm-3">';
         	  msg += '<ul class="login_details1">';
         		msg += '<li><a href="/viewprofile?uid='+d.uid+'">'+d.FirstName+' '+d.SurName+'</a></li>'
         		msg += '<li><p></p></li></ul>'
         	  msg += '</div>'
         	  msg += '<div class="col-sm-6">'
         	  msg += '<table class="table_working_hours">'
         	  msg += '<tbody>'
            msg += '<tr class="opened_1">'
            msg += '<td class="day_label1">First Name :</td>'
            msg += '<td class="day_value">'+d.FirstName+'</td></tr>'
 				    msg += '<tr class="opened"><td class="day_label1">Last Name :</td><td class="day_value">'+d.SurName+'</td></tr>';
         	  msg += '<tr class="opened"><td class="day_label1">Age :</td><td class="day_value">'+calculateage(d.Dob)+'</td></tr>'
         		msg += '<tr class="opened"><td class="day_label1">Marital Status :</td><td class="day_value">'+d.MaritialStatus+'</td></tr>'
         	//	msg += '<tr class="opened"><td class="day_label1">Sub Caste :</td><td class="day_value">'+d.sub_cast+'</td></tr>'
         		msg += '<tr class="closed"><td class="day_label1">Profile Created by :</td><td class="day_value closed"><span>'+d.ProfileCreatedBy+'</span></td></tr>';
            msg += '</tbody>'
         		msg += '</table>';
         		//msg += '<div class="buttons"><div class="vertical">Send Mail</div>';
            //msg += '<div class="vertical">Shortlisted</div>'
         		//msg += '<div class="vertical">Send Interest</div>'
             msg += '</div></div><div class="clearfix"> </div></a></div><br>';
          //   msg += '<button onclick=actioncall("'+d._id+'","'+d.uid+'","'+d.FirstName+'","'+d.SurName+'") type="button" class="btn btn-primary btn-lg btn-block">Click Here To Show Your Interests</button>'
          })
          $("#newmatches").html(msg);
        }else{
          msg += "No Records Found.."
        }
      }
  });
}
function getnewprofiles(){
  var data = {};
  data.page = 1;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/Newgetprofiles',
    async:true,
    crossDomain:true,
      success: function(res) {
        var msg = '';
        if(res.length>0){
          $.each(res,function(i,d){
            msg += '<div class=""><a href="/viewprofile?uid='+d.uid+'"><h2>'+d.matrimony_id+'</h2>';
            msg += '<div class="col-sm-3 profile_left-top">';
           if (d.Photo1 == undefined || d.Photo1 == "") {
             var img = "Public/images/profile_photos/noimage.jpeg"
           }else {
             var img = "Public/images/profile_photos/"+d.Photo1;
           }
           msg += '<a href="/viewprofile?uid='+d.uid+'"><img src="'+img+'" class="img-responsive profileimage" alt=""/></a>';
           msg += '</div>'
           msg += '<div class="col-sm-3">';
            msg += '<ul class="login_details1">';
            msg += '<li><a href="/viewprofile?uid='+d.uid+'">'+d.FirstName+' '+d.SurName+'</a></li>'
            msg += '<li><p></p></li></ul>'
            msg += '</div>'
            msg += '<div class="col-sm-6">'
            msg += '<table class="table_working_hours">'
            msg += '<tbody>'
           msg += '<tr class="opened_1">'
           msg += '<td class="day_label1">First Name :</td>'
           msg += '<td class="day_value">'+d.FirstName+'</td></tr>'
            msg += '<tr class="opened"><td class="day_label1">Last Name :</td><td class="day_value">'+d.SurName+'</td></tr>';
            msg += '<tr class="opened"><td class="day_label1">Age :</td><td class="day_value">'+calculateage(d.Dob)+'</td></tr>'
            msg += '<tr class="opened"><td class="day_label1">Marital Status :</td><td class="day_value">'+d.MaritialStatus+'</td></tr>'
          //	msg += '<tr class="opened"><td class="day_label1">Sub Caste :</td><td class="day_value">'+d.sub_cast+'</td></tr>'
            msg += '<tr class="closed"><td class="day_label1">Profile Created by :</td><td class="day_value closed"><span>'+d.ProfileCreatedBy+'</span></td></tr>';
           msg += '</tbody>'
            msg += '</table>';
            //msg += '<div class="buttons"><div class="vertical">Send Mail</div>';
           //msg += '<div class="vertical">Shortlisted</div>'
            //msg += '<div class="vertical">Send Interest</div>'
            msg += '</div></div><div class="clearfix"> </div></a></div><br>';
           // msg += '<button onclick=actioncall("'+d._id+'","'+d.uid+'","'+d.FirstName+'","'+d.SurName+'") type="button" class="btn btn-primary btn-lg btn-block">Click Here To Show Your Interests</button>'
            // "'+d._id+'","'+d.matrimony_id+'","'+d.uid+'","'+d.FirstName+'","'+d.SurName+'","'+d.Photo1+'","'+d.Dob+'","'+d.MaritialStatus+'","'+d.ProfileCreatedBy+'"
          })
          $("#newmatches").html(msg);
        }else{
          msg += "No Records Found.."
        }
      }
  });
}


function getprofileslenght(){
  $.ajax({
    type: 'POST',
    contentType: 'application/json',
    url: '/getprofileslength',
    async:true,
    crossDomain:true,
      success: function(res) {
        console.log(res)
        var lenght = res.length;
        var remaining = lenght%25;
        var pag = lenght-remaining;
        var noofpages = pag/25;
        var msg = '';
        for (var i = 1; i < noofpages; i++) {
          msg += '<li ><a href="JavaScript:Void(0)" onclick="getprofiles('+i+')">'+i+'</a></li>';
        }
        $("#pagination").html(msg);
      }
  });
}




function getphotos(uid){
  var data = {};
  data.uid = uid;
  var msg = '';
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getphotosf',
    async:false,
    crossDomain:true,
      success: function(res) {
        console.log(res);
      }
  });
  return msg;
}

function getloggerinfo(){
  $.ajax({
    type: 'POST',
    contentType: 'application/json',
    url: '/getsessioninfo',
    async:true,
    crossDomain:true,
      success: function(res) {
        console.log(res);
        if (res.Photo1 == "") {
          $("#photouploadmodal").modal("show");
        }
        if (res.PaymentStatus != "1") {
          
          return false;
        }else{

        }
      }
  });
}

function getsession(){
  $.ajax({
    type: 'POST',
    contentType: 'application/json',
    url: '/getsessioninfo',
    async:true,
    crossDomain:true,
      success: function(res) {
        $("#userid").val(res._id);
        $("#useremail").val(res.mem_email);
        $("#username").val(res.first_name);
        $("#mobilenumber").val(res.mobile);
      }
  });
}


function calculateage(dob){
  var dateTime = moment(dob).format("YYYY-MM-DD");
  var years = moment().diff(dateTime, 'years',false);
  return years;
}

$("#serchbyid").click(function(){
  var data = {};
  data.memberid = $("#membershipid").val();
})
