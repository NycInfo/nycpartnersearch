
      var mongoose = require('mongoose');

      var bgschema = mongoose.Schema({
        bgimage:'string',
        Status:'String',
        updateddate: { type: Date, default: Date.now },
      });
      var site_frmschema = mongoose.Schema({
        BG_pic:'string',
        Bride_Name:'string',
        Groom_Name:'string',
        Description:'string',
        Status:'String',
        Registered_Date: { type: Date, default: Date.now },
      });
      var theme_schema = mongoose.Schema({
          BG_pic:'string',
          Registered_Date: { type: Date, default: Date.now },
      });
       
        var kycschema = mongoose.Schema({
          userid:'string',
          Aadhar:'string',
          PANCard:'string',
          Status:'String',
          Registered_Date: { type: Date, default: Date.now },
        });
        var EventSchema =new mongoose.Schema({
          event_pic: "string",
          eventname:"string",
          eventdescr:"string",
          street:"string",
          city:"string",
          state:"string",
          landmark:"string",
          dateofevent:"string",
          Status:'string',
          CreateDate:{ type: Date, default: Date.now },
        });
        var adminSchema = mongoose.Schema({
          id:Number,
          username:"string",
          password:"string",
          firstName:"string",
          lastName:"string",
          email:"string",
          mobile:"string",
          Role:"string",
          Status:"string",
          Commision:Number,
          Address:"string",
          Wallet:"String"
        });
          
          
            var favouritesSchema = mongoose.Schema({
              Touid:"String",
              Fromuid:"String",
              ToProfileId:"string",
              FromProfileId:"string",
              ToMatrimonyId:"string",
              FromMatrimonyId:"string",
              ToName:"string",
              FromName:"string",
              Dob:"string",
              MaritialStatus:"string",
              CreatedBy:"string",
              Status:"String",
              StatusMessage:"String",
              Response:"String",
              favourite_date:{ type: Date, default: Date.now },
            })
            
            var intrestsSchema = mongoose.Schema({
              Touid:"String",
              Fromuid:"String",
              ToProfileId:"string",
              FromProfileId:"string",
              ToMatrimonyId:"string",
              FromMatrimonyId:"string",
              ToName:"string",
              FromName:"string",
              Dob:"string",
              MaritialStatus:"string",
              CreatedBy:"string",
              Status:"String",
              StatusMessage:"String",
              Response:"String",
              favourite_date:{ type: Date, default: Date.now },
            })

            var matrimonyprofilesSchema =new mongoose.Schema({
              uid: Number,
              matrimony_id:"String",
              ProfileCreatedBy:"String",
              FirstName:"String",
              SurName:"String",
              Gender:"String",
              Email:"String",
              Password:"String",
              Mobile:"String",
              Dob:"String",
              Age:Number,
              MaritialStatus:"String",
              Religion:"String",
              Caste:"String",
              MotherTongue:"String",
              Country:"String",
              Timeofbirth:"String",
              Placeofbirth:"String",
              Star:"String",
              Padam:"String",
              Rasi:"String",
              Kujadosam:"String",
              Gothram:"String",
              Height:"String",
              Weight:"String",
              Complexion:"String",
              Education:"String",
              Occupation:"String",
              WorkingIn:"String",
              Salary:"String",
              Origin:"String",
              FatherName:"String",
              FatherOccupation:"String",
              MotherName:"String",
              MotherOccupation:"String",
              Brothers:"String",
              Sisters:"String",
              MarriedBrothers:"String",
              MarriedSisters:"String",
              Address:"String",
              Village:"String",
              City:"String",
              State:"String",
              Country:"String",
              Reference:"String",
              AlternameMobile:"String",
              FamilyDetails:"String",
              PropertyDetails:"String",
              P_PartnerAgeFrom:"String",
              P_PartnerAgeTo:"String",
              P_PartnerHeightFrom:"String",
              P_PartnerHeightFromTo:"String",
              P_PartnerComplexion:"String",
              P_PartnerMaritalStatus:"String",
              P_PartnerHaveChildren:"String",
              P_PartnerDosam:"String",
              P_PartnerMotherTongue:"String",
              P_PartnerFamilyValue:"String",
              P_P_PartnerEmployedAs:"String",
              P_PartnerAnnuIncome :"String",
              P_PartnerPropertyRange:"String",
              P_FamilyProperty :"String",
              P_FamilyPropertyDetails:"String",
              P_AboutPartner:"String",
              Photo1:"String",
              Photo2:"String",
              Photo3:"String",
              Photo4:"String",
              PaymentStatus:"String",
              LastLogin:"String",
              ProfileStatus:"String",
              Status:"String",
              views:Number,
              CreateDate:{ type: Date, default: Date.now },
              UpdatedDate:{ type: Date, default: Date.now }
            });
            
           
           
            var registerescountschema = new mongoose.Schema({
              registernumber:Number,
              matrimonynumber:"String",
              lastcreated:{ type: Date, default: Date.now },
              Status:"string",
            })

            var profileactivitySchema = new mongoose.Schema({
              uid:"string",
              mid : "string",
              Name : "string",
              mobile : "string",
              actiontype:"string",
              actionto:"string",
              actiontime:{ type: Date, default: Date.now },
              Status:"string"
            })


            var regcount = mongoose.model('reg_count', registerescountschema, "reg_count");
            var profileactivity = mongoose.model('profileactivity', profileactivitySchema, "profileactivity");

            var events = mongoose.model('eventsandnews', EventSchema, "eventsandnews");
            var admins = mongoose.model('admin', adminSchema, "admin");
            var intrests = mongoose.model('intrests', intrestsSchema, "intrests");
            var favourites =  mongoose.model('favourites', favouritesSchema, "favourites");
            var matrimony_profile = mongoose.model('profiles', matrimonyprofilesSchema, "profiles");
            var succes_stories = mongoose.model('succes_stories', site_frmschema);
            var themes = mongoose.model('themes', theme_schema);
            var user_kycs = mongoose.model('user_kycs', kycschema);
            var bgschema = mongoose.model('bgschema', bgschema);
            module.exports = {
              port : process.env.PORT || 8080,
              url : 'mongodb://localhost:27017/nycpartnersearch',
              //url : 'mongodb://localhost:27017/NewMatrimony',
              mongo : mongoose,
              //cmsImgPath :'/opt/Projects/vishwakarma/Public/images/profile_photos', //need to change live
              cmsImgPath :'Public/images/profile_photos',
              adminurl : 'https://nyclifeline.com',
              bgPath : '/opt/Projects/vishwakarma/Public2/img/themes',
              regcount:regcount,
              profileactivity:profileactivity,
              events:events,
              admins : admins,
              matrimony_profile : matrimony_profile,
              intrests:intrests,
              favourites:favourites,
              background : bgschema,
              succes_stories : succes_stories,
              themes : themes,
              user_kycs : user_kycs,
            }
