var path = require('path');
var session = require('express-session');
var hart = require('../hart');
var async = require('async');
var crypto = require('crypto');
var hart = require('../hart');
var url = require('url');
var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var globals = require('node-global-storage');
var request = require('request');
var ObjectId = require('mongodb').ObjectID;

var auth = function (req, res, next) {
  if (req.session.UserId != "" && req.session.user)
    return next();
  else
    return res.redirect('/');
};
var auth1 = function (req, res, next) {
  if (req.session.AdminId != "" && req.session.Admin)
    return next();
  else
    return res.redirect('/Admin');
};
module.exports = function (app) {

  app.use(session({
    secret: 'vshwkrm',
    resave: true,
    saveUninitialized: true
  }));


  app.post('/checkingemailisregisteredalready', function (req, res) {
    //{ "Email": req.body.emailentered }
    hart.matrimony_profile.count({ "$and": [{ "Status": "1" }, { "$or": [{ "Mobile": req.body.phoneentered }] }] }).exec(function (err, doc) {
      if (!err) {
        res.json(doc);
      } else {
        res.json("0");

      }
    });
  })
  app.post('/Profilelogin', function (req, res) {

    hart.matrimony_profile.find({ "$and": [{ "Password": req.body.password }, { "Status": "1" }, { "$or": [{ "Mobile": req.body.username }, { "matrimony_id": req.body.username }, { "Email": req.body.username }] }] }).exec(function (err, doc) {
      if (!err) {
        if (doc.length > 0) {
          req.session.UserId = doc[0]._id;
          req.session.uid = doc[0].uid;
          req.session.username = doc[0].FirstName + ' ' + doc[0].SurName;
          req.session.matrimony_id = doc[0].matrimony_id;
          req.session.Email = doc[0].Email;
          req.session.Gender = doc[0].Gender;
          req.session.Mobile = doc[0].Mobile;
          req.session.user = true;
          console.log(req.session.matrimony_id, req.session.username);
          var pactivty = hart.profileactivity;
          var profileactivity = new pactivty({
            uid:req.session.uid,
            mid: req.session.matrimony_id,
            Name: req.session.username,
            mobile: req.session.Mobile,
            actiontype: "Login",
            actionto: "",
            Status: "1"
          });
          profileactivity.save(function (err, results) {
            if (err) {
              res.json(0);
            } else {
              res.json(1);
            }
          });
        } else {
          res.json("0");
        }
      } else {
        res.json("0");
      }
    });
  })

  app.post('/getsessioninfo', function (req, res) {
    var id = ObjectId(req.session.UserId);
    var Query = { "_id": id };
    hart.matrimony_profile.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        //    console.log(data);
        res.json(data);
      }
    });
  })

  app.post('/adminlogin', function (req, res) {
    var collection2 = hart.admins;
    collection2.find({ "username": req.body.userid }).exec(function (err, doc1) {
      if (doc1 == "") {
        res.end(JSON.stringify(0));
      } else {
        if (doc1[0].password == req.body.password) {
          req.session.AdminId = doc1[0]._id;
          req.session.AdminName = doc1[0].username;
          req.session.AdminEmail = doc1[0].email;
          req.session.Role = doc1[0].Role;
          req.session.Wallet = doc1[0].Wallet;
          req.session.Admin = true;
          res.end(JSON.stringify(1));
        }
        else {
          res.end(JSON.stringify(0));
        }
      }
    });
  });

  app.get('/logout', function (req, res) {
    delete req.session.user;
    req.session.destroy();
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.sendFile(path.resolve(__dirname, '../Public/index.html'));
  });
  app.get('/Index', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/index.html'));
  });
  app.get('/Login', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/login.html'));
  });
  app.get('/forgotpassword', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/forgotpassword.html'));
  });
  app.get('/Registration', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/register.html'));
  });
  app.get('/home', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/home.html'));
  });
  app.get('/inbox', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/inbox.html'));
  });
  app.get('/My_Profile', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/my_profile.html'));
  });
  app.get('/Contact_us', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/contact.html'));
  });
  app.get('/About_us', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/about.html'));
  });
  app.get('/MyFavs', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/favourites.html'));
  });
  app.get('/New_Profiles', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/matches.html'));
  });
  app.get('/upgrade', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/upgrade.html'));
  });
  app.get('/viewprofile', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/view_profile.html'));
  });
  app.get('/edit_profile', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/editprofile.html'));
  });
  app.get('/managephotos', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/managephotos.html'));
  });
  app.get('/Services', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/services.html'));
  });
  app.get('/presidentmsg', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/presidentmessage.html'));
  });
  app.get('/SearchResult', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/searchResults.html'));
  });
  app.get('/Findprofiles', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/indexsearch.html'));
  });
  app.get('/SentInterests', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/sentinterests.html'));
  });
  app.get('/ReceivedInterests', auth, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/receivedinterests.html'));
  });
  
  app.get('/Admin', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/login.html'));
  });
  app.get('/AdminDashboard', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/index.html'));
  });
  app.get('/AllProfiles', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/all-profiles.html'));
  });
  app.get('/AllGrooms', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/allgrooms.html'));
  });
  app.get('/AllBrides', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/allbrides.html'));
  });
  app.get('/DeletedProfiles', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/deletedprofiles.html'));
  });
  app.get('/PaidProfiles', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/paidusers.html'));
  });
  app.get('/UnpaidProfiles', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/unpaidusers.html'));
  });
  app.get('/NewRegistration', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/Createprofile.html'));
  });
  app.get('/viewpro_admin', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/viewprofilebyadmin.html'));
  });
  app.get('/SiteManage', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/sitemanage.html'));
  });
  app.get('/viewpro_admin', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/viewprofilebyadmin.html'));
  });
  app.get('/Admineditprofile', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/admineditprofile.html'));
  });
  app.get('/ProfileReg_Admin', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/adminregister.html'));
  });
  app.get('/ProfileActivities', auth1, function (req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/profileactivities.html'));
  });

}
