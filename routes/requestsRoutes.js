var async = require('async');
var hart = require('../hart');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer = require('multer');
var nodemailer = require('nodemailer');
var request = require("request");
var ObjectId = require('mongodb').ObjectID;
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, hart.cmsImgPath)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

var upload = multer({ storage: storage });

module.exports = function (app) {

  app.use(session({
    secret: 'Niharinfo',
    resave: true,
    saveUninitialized: true
  }));

  app.post('/getfourites', function (req, res) {
    if (req.body.Status == "3") {
      hart.favourites.find({ "$and": [{ "ToProfileId": req.session.UserId }, { "Status": "2" }] }).exec(function (err, doc) {
        if (!err) {
          res.json(doc)
        } else {
          console.log(err)
        }
      })
    } else {
      hart.favourites.find({ "$and": [{ "FromProfileId": req.session.UserId }, { "Status": req.body.Status }] }).exec(function (err, doc) {
        if (!err) {
          res.json(doc)
        } else {
          console.log(err)
        }
      })
    }

  })

  app.post('/removefourites', function (req, res) {
    var id = ObjectId(req.body.id);
    hart.favourites.remove({ '_id': id }).exec(function (err, doc) {
      if (!err) {
        res.json("1")
      } else {
        res.json("0")
      }
    })
  })

  app.post('/saveinfavs', function (req, res) {
    hart.favourites.find({ "$and": [{ "ToProfileId": req.body.profilerowid }, { "FromProfileId": req.session.UserId }, { "Status": req.body.Status }] }).exec(function (err, doc) {
      if (!err) {
        if (doc.length > 0) {
          res.json("This Profile Already In Your List");
        } else {
          var id = ObjectId(req.body.profilerowid);
          hart.matrimony_profile.find({ "$and": [{ "_id": id }, { "Status": "1" }] }).exec(function (err, pdata) {
            if (!err) {
              var favs = hart.favourites;
              var favorites = new favs({
                Touid: pdata[0].uid,
                Fromuid: req.session.uid,
                ToProfileId: req.body.profilerowid,
                FromProfileId: req.session.UserId,
                ToMatrimonyId: pdata[0].matrimony_id,
                FromMatrimonyId: req.session.matrimony_id,
                ToName: pdata[0].FirstName + ' ' + pdata[0].SurName,
                FromName: req.session.username,
                Dob: pdata[0].Dob,
                MaritialStatus: pdata[0].MaritialStatus,
                CreatedBy: pdata[0].ProfileCreatedBy,
                Status: req.body.Status,
                StatusMessage: req.body.statusmessage,
                Response: "",
              });
              favorites.save(function (err1, results) {
                if (!err1) {
                  res.json("Thank you..It's Done")
                  if (req.body.Status == "2") {
                    var atype = "Show interest"
                  } else if (req.body.Status == "1") {
                    var atype = "Fav Inn"
                  } else {
                    var atype = "Requesting Contact"
                  }
                  var pactivty = hart.profileactivity;
                  var profileactivity = new pactivty({
                    uid: req.session.uid,
                    mid: req.session.matrimony_id,
                    Name: req.session.username,
                    mobile: req.session.Mobile,
                    actiontype: atype,
                    actionto: pdata[0].matrimony_id,
                    Status: "1"
                  });
                  profileactivity.save(function (err, results) {
                    if (err) {
                    } else {
                    }
                  });
                } else {
                  res.json("Please Try Later");
                }
              });
            } else {
              console.log(err)
            }
          })
        }
      } else {
        console.log(err);
      }
    })
  })



  function sendsms(number, message) {
    var txt = message;
    var options = {
      method: 'GET',
      url: 'https://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
      qs:
      {
        type: 'smsquicksend',
        user: 'nycinfo',
        pass: 'Xun6ah9F',
        sender: 'VSKM',
        to_mobileno: number,
        sms_text: txt
      },
      headers:
      {
        'cache-control': 'no-cache'
      }
    };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });
  }


}
