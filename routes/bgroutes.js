var async = require('async');
var hart= require('../hart');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var nodemailer = require('nodemailer');
var ObjectId = require('mongodb').ObjectID;
var nodemailer = require('nodemailer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.bgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });
module.exports = function(app){

  app.use(session({
  secret: 'Niharinfo',
  resave: true,
  saveUninitialized: true
}));

    app.post('/AddSuccessstory',upload.any(), function(req,res){
                var  bg_pic=req.files[0].filename;
                var sitedata = hart.succes_stories;
                var successsss = new sitedata({
                  BG_pic: bg_pic,
                  Bride_Name:req.body.Bride_Name,
                  Groom_Name:req.body.Groom_Name,
                  Description:req.body.Description,
                  Status:'1',
                });
              successsss.save(function (err, results) {
                if (err) {
                  res.json(1);
                }else {
                  res.json(0);
                }
              });
      })

      app.post('/AddEventsNews',upload.any(), function(req,res){
                  var  eve_pic = req.files[0].filename;
                  var Events = hart.events;
                  var eventsss = new Events({
                    event_pic: eve_pic,
                    eventname:req.body.eventname,
                    eventdescr:req.body.eventdescr,
                    street:req.body.street,
                    city:req.body.city,
                    state:req.body.state,
                    landmark:req.body.landmark,
                    dateofevent:req.body.dateofevent,
                    Status:'1',
                  });
                eventsss.save(function (err, results) {
                  if (err) {
                    res.json(1);
                  }else {
                    res.json(0);
                  }
                });
        })

    app.post('/Upload_theme',upload.any(), function(req,res){
          var  bg_pic=req.files[0].filename;
                var sitedata = hart.themes;
                var sitedf = new sitedata({
                  BG_pic: bg_pic,
                  Status:'1',
                });
              sitedf.save(function (err, results) {
                if (err) {
                  res.json(1);
                }else {
                  res.json(0);
                }
              });
      })
      app.post('/getbgtheme', function(req,res){
          var collection =  hart.themes;
          collection.find().sort({Registered_Date:-1}).exec(function(err, data) {
            if(err){
              console.log(err);
            }else {
                res.json(data);
              }
          })
      })
      app.post('/success_stories', function(req,res){
          var collection = hart.succes_stories;
          collection.find({"Status":"1"}).sort({Registered_Date:-1}).exec(function(err, data) {
            if(err){
              console.log(err);
            }else {
                res.json(data);
              }
          })
      })
      app.post('/eventfunctions', function(req,res){
          var collection = hart.events;
          collection.find({"Status":"1"}).sort({Registered_Date:-1}).exec(function(err, data) {
            if(err){
              console.log(err);
            }else {
                res.json(data);
              }
          })
      })

}
