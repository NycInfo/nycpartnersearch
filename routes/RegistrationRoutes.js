var async = require('async');
var hart = require('../hart');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer = require('multer');
var nodemailer = require('nodemailer');
var fs = require('fs');
var ObjectId = require('mongodb').ObjectID;
var nodemailer = require('nodemailer');
var request = require('request');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, hart.cmsImgPath)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

var upload = multer({ storage: storage });

module.exports = function (app) {

  app.use(session({
    secret: 'Niharinfo',
    resave: true,
    saveUninitialized: true
  }));



  app.post('/updatepassword', function (req, res) {
    var id = ObjectId(req.session.UserId);
    var collection = hart.users;
    collection.updateOne({ '_id': id }, { $set: { Password: req.body.newpassword } }, function (err, response) {
      if (err) {
        console.log(err);
        res.json(0);
      }
      else {
        res.json(1);
      }
    });
  });

  app.post('/Registration', function (req, res) {
    var users = hart.matrimony_users;
    var Profiles = hart.matrimony_profile;
    var regcount = hart.regcount;
    regcount.findOne({ Status: "1" }, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        var regnum = data.registernumber + 1;
        if (req.body.gender == "MALE") {
          var regmatid = "NYCG" + regnum;
        } else {
          var regmatid = "NYCB" + regnum;
        }
        regcount.updateOne({ 'Status': "1" }, { $set: { registernumber: regnum, matrimonynumber: regmatid } }, function (err, response) {
          if (err) {
            console.log(err);
          }
          else {
            var Uid = regnum;
            var mat_id = regmatid;
            var dateofb = req.body.dob_year + "-" + req.body.dob_month + "-" + req.body.dob_date + " 00:00:00.000";
            var years = moment().diff(req.body.dob_year + "-" + req.body.dob_month + "-" + req.body.dob_date, 'years');
            var profiless = new Profiles({
              uid: Uid,
              matrimony_id: mat_id,
              ProfileCreatedBy: req.body.profilefor,
              FirstName: req.body.firstname,
              SurName: req.body.lastname,
              Gender: req.body.gender,
              Email: req.body.email,
              Password: req.body.password,
              Mobile: req.body.mobilenumber,
              Dob: dateofb,
              Age: years,
              MaritialStatus: req.body.maritalstatus,
              Religion: req.body.religion,
              Caste: req.body.caste,
              MotherTongue: req.body.mothertongue,
              Country: req.body.country,
              Timeofbirth: req.body.timeofbirth,
              Placeofbirth: req.body.placeofbirth,
              Star: req.body.star,
              Padam: req.body.padam,
              Rasi: req.body.rasi,
              Kujadosam: req.body.kujadosam,
              Gothram: req.body.gothram,
              Complexion: req.body.complexian,
              Height: req.body.height,
              Weight: req.body.weight,
              Education: req.body.Education,
              Occupation: req.body.Occupation,
              WorkingIn: req.body.Working,
              Salary: req.body.Salary,
              Origin: req.body.ancestralorigin,
              FatherName: req.body.fathername,
              FatherOccupation: req.body.fatheroccupation,
              MotherName: req.body.mothername,
              MotherOccupation: req.body.motheroccupation,
              Brothers: req.body.brothers,
              Sisters: req.body.sisters,
              MarriedBrothers: req.body.marriedbrothers,
              MarriedSisters: req.body.marriedsisters,
              Address: req.body.Address,
              Village: req.body.Village,
              City: req.body.City,
              State: req.body.State,
              Country: req.body.Country,
              Reference: req.body.Reference,
              AlternameMobile: req.body.alternatemobile,
              FamilyDetails: req.body.familydetails,
              PropertyDetails: req.body.propertydetails,
              P_PartnerAgeFrom: req.body.P_PartnerAgeFrom,
              P_PartnerAgeTo: req.body.P_PartnerAgeTo,
              P_PartnerHeightFrom: req.body.P_PartnerHeightFrom,
              P_PartnerHeightFromTo: req.body.P_PartnerHeightFromTo,
              P_PartnerComplexion: req.body.P_PartnerComplexion,
              P_PartnerMaritalStatus: req.body.P_PartnerMaritalStatus,
              P_PartnerHaveChildren: req.body.P_PartnerHaveChildren,
              P_PartnerDosam: req.body.P_PartnerDosam,
              P_PartnerMotherTongue: req.body.P_PartnerMotherTongue,
              P_PartnerFamilyValue: req.body.P_PartnerFamilyValue,
              P_P_PartnerEmployedAs: req.body.P_P_PartnerEmployedAs,
              P_PartnerAnnuIncome: req.body.P_PartnerAnnuIncome,
              P_PartnerPropertyRange: req.body.P_PartnerPropertyRange,
              P_FamilyProperty: req.body.P_FamilyProperty,
              P_FamilyPropertyDetails: req.body.P_FamilyPropertyDetails,
              P_AboutPartner: req.body.P_AboutPartner,
              Photo1: "",
              Photo2: "",
              Photo3: "",
              Photo4: "",
              PaymentStatus: "",
              ProfileStatus: "1",
              Status: "1"
            });
            profiless.save(function (err, results) {
              if (err) {
                console.log(err);
                res.json(0);
              }
              else {
                res.json(1);
                const body = 'Welcome to Nyc Life Line Services. <br/> Your Matrimony Id is ' + mat_id + ' and password is ' + req.body.password + ' <br/> All the best. For see the profiles https://www.nyclifeline.com ';
                if (req.body.emailnotification == "Yes") {
                  sendmail(req.body.email, "NYC LIFE LINE - " + mat_id, body);
                }else{
                  sendmail("nyclifeline.com", "NYC LIFE LINE - " + mat_id, body);
                }
                sendsms(req.body.mobilenumber, "Dear " + req.body.firstname + ", Thank you for registering with NYC. Your ID is " + mat_id + " and password is " + req.body.password + ". Wishing you luck in your partner search. for login: https://nyclifeline.com/");
              }
            });

          }
        });
      }
    });
  });

  app.post('/EditProfile', function (req, res) {
    if (req.body.source == "admin") {
      var id = req.body.uid;
    } else {
      var id = req.session.uid;
    }
    var dateofb = req.body.dob_year + "-" + req.body.dob_month + "-" + req.body.dob_date + " 00:00:00.000";
    var years = moment().diff(req.body.dob_year + "-" + req.body.dob_month + "-" + req.body.dob_date, 'years');

    hart.matrimony_profile.updateOne({ 'uid': id }, {
      $set: {
        ProfileCreatedBy: req.body.profilefor,
        FirstName: req.body.firstname,
        SurName: req.body.lastname,
        Gender: req.body.gender,
        Email: req.body.email,
        Password: req.body.password,
        Mobile: req.body.mobilenumber,
        Dob: dateofb,
        Age: years,
        MaritialStatus: req.body.maritalstatus,
        Religion: req.body.religion,
        Caste: req.body.caste,
        MotherTongue: req.body.mothertongue,
        Country: req.body.country,
        Timeofbirth: req.body.timeofbirth,
        Placeofbirth: req.body.placeofbirth,
        Star: req.body.star,
        Padam: req.body.padam,
        Rasi: req.body.rasi,
        Kujadosam: req.body.kujadosam,
        Gothram: req.body.gothram,
        Complexion: req.body.complexian,
        Height: req.body.height,
        Weight: req.body.weight,
        Education: req.body.Education,
        Occupation: req.body.Occupation,
        WorkingIn: req.body.Working,
        Salary: req.body.Salary,
        Origin: req.body.ancestralorigin,
        FatherName: req.body.fathername,
        FatherOccupation: req.body.fatheroccupation,
        MotherName: req.body.mothername,
        MotherOccupation: req.body.motheroccupation,
        Brothers: req.body.brothers,
        Sisters: req.body.sisters,
        MarriedBrothers: req.body.marriedbrothers,
        MarriedSisters: req.body.marriedsisters,
        Address: req.body.Address,
        Village: req.body.Village,
        City: req.body.City,
        State: req.body.State,
        Country: req.body.Country,
        Reference: req.body.Reference,
        AlternameMobile: req.body.alternatemobile,
        FamilyDetails: req.body.familydetails,
        PropertyDetails: req.body.propertydetails,
        P_AboutPartner: req.body.partnerpreference
      }
    }, function (err, response) {
      if (err) {
        console.log(err);
        res.json("0")
      }
      else {
        res.json("1")
        console.log("ok")
      }
    });
  })

  function randfunc(number) {
    var chars = number;
    var string_length = 6;
    var randomstring = '';
    var charCount = 0;
    var numCount = 0;
    for (var i = 0; i < string_length; i++) {
      if ((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
        var rnum = Math.floor(Math.random() * 10);
        randomstring += rnum;
        numCount += 1;
      } else {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
        charCount += 1;
      }
    }
    return randomstring;
  }

  function sendmail(tomail, subject, msg) {
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      // host: 'smtp.gmail.com',
      // port: 465,
      // secure: true,
      auth: {
        user: 'nyclifeline@gmail.com',
        pass: 'Nani5953@'
      }
    });
    var mailOptions = {
      from: 'nyclifeline@gmail.com',
      to: tomail,
      subject: subject,
      html: msg
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("mail sent");
      }
    });
  }

  function sendsms(number, message) {
    var txt = message;
    var options = {
      method: 'GET',
      url: 'https://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
      qs:
      {
        type: 'smsquicksend',
        user: 'nycinfo',
        pass: 'Xun6ah9F',
        sender: 'NYCINF',
        campaign_name:'NYC Life Line',
        to_mobileno: number,
        sms_text: txt
      },
      headers:
        { 'cache-control': 'no-cache' }
    };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });
  }

}
