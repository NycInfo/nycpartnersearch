var async = require('async');
var hart = require('../hart');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer = require('multer');
var nodemailer = require('nodemailer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, hart.cmsImgPath)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

var upload = multer({ storage: storage });

module.exports = function (app) {

  app.use(session({
    secret: 'Niharinfo',
    resave: true,
    saveUninitialized: true
  }));









  app.post('/getprofiledetailsfromusers', function (req, res) {
    var Query = { "uid": req.body.uid };
    hart.matrimony_profile.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        //    console.log(data);
        res.json(data);
      }
    });
  })
  app.post('/getsubcastewithid', function (req, res) {
    var Query = { "sc_id": req.body.dtt };
    hart.subcaste.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        res.json(data);
      }
    });
  })
  app.post('/getstartwithid', function (req, res) {
    var Query = { "pk_i_id": req.body.star };
    hart.birth_star.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        res.json(data);
      }
    });
  })
  app.post('/getrasitwithid', function (req, res) {
    console.log(req.body.rasi);
    var Query = { "pk_i_id": req.body.rasi };
    hart.raasi.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        res.json(data);
      }
    });
  })
  app.post('/geteducationtwithid', function (req, res) {
    var Query = { "education_table_id": req.body.educ };
    hart.education_table.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        res.json(data);
      }
    });
  })
  app.post('/getprofiledetailsfromprofile', function (req, res) {
    var Query = { "uid": req.body.uid };
    hart.matrimony_profile.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        //    console.log(data);
        res.json(data);
      }
    });
  })
  app.post('/latestprofiles', function (req, res) {

    var Query = { "$and": [{ "Status": "1" }] };
    hart.matrimony_profile.find(Query).sort({ _id: -1 }).limit(25).exec(function (err, doc1) {
      if (err) {
        console.log(err);
      } else {
        res.json(doc1)
      }
    });
  })
  
  app.post('/Indexsearch', function (req, res) {
    console.log(req.body)
    var Query = { "$and": [{ "Gender": req.body.gender },{ "Religion": req.body.religion },{ "Caste": req.body.caste },{ "Status": "1" },{"MaritialStatus":req.body.maritalstatus}] };
    hart.matrimony_profile.find(Query).sort({ _id: -1 }).limit(50).exec(function (err, doc1) {
      if (err) {
        console.log(err);
      } else {
        res.json(doc1)
      }
    });
  })




  app.post('/getmyprofiledetailsfromusers', function (req, res) {
    console.log(req.body)
    if (req.body.source == 'admin') {
      var Query = { "uid": req.body.uid };
    } else {
      var Query = { "uid": req.session.uid };
    }
    hart.matrimony_profile.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        //    console.log(data);
        res.json(data);
      }
    });
  })

  app.post('/getmyprofiledetailsfromprofile', function (req, res) {
    if (req.body.source == 'admin') {
      var Query = { "uid": req.body.uid };
    } else {
      var Query = { "uid": req.session.uid };
    }
    hart.matrimony_profile.findOne(Query, function (err, data) {
      if (err) {
        console.log(err);
      }
      else {
        res.json(data);
      }
    });
  })




  app.post('/Newgetprofiles', function (req, res) {
    if (req.session.Gender == "MALE") {
      var Gender = "FEMALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };

    } else {
      var Gender = "MALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };
    }
    if (req.body.pagination != "") {
      var skipby = parseInt(req.body.pagination);
    } else {
      var skipby = 0;
    }
    hart.matrimony_profile.find(Query).sort({ _id: -1 }).skip(skipby).limit(25).exec(function (err, doc1) {
      if (err) {
        console.log(err);
      } else {
        res.json(doc1)
      }
    });
  })
  app.post('/getfavouriteprofiles', function (req, res) {
    var Query = { "$and": [{ "ProfileId": req.session.UserId }, { "Status": "1" }] };
    hart.favourites.find(Query).sort({ _id: -1 }).exec(function (err, doc1) {
      if (err) {
        console.log(err);
      } else {
        res.json(doc1)
      }
    });
  })

  app.post('/getprofiles', function (req, res) {
    if (req.session.Gender == "MALE") {
      var Gender = "FEMALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };

    } else {
      var Gender = "MALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };
    }
    if (req.body.pagination != "") {
      var skipby = parseInt(req.body.pagination) * 25;
    } else {
      var skipby = 0;
    }

    hart.matrimony_profile.find(Query).sort({ _id: -1 }).skip(skipby).limit(25).exec(function (err, doc1) {
      if (err) {
        console.log(err);
      } else {
        res.json(doc1)
      }
    });
  })
  app.post('/getprofileslength', function (req, res) {
    if (req.session.Gender == "MALE") {
      var Gender = "FEMALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };

    } else {
      var Gender = "MALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };
    }
    if (req.body.pagination != "") {
      var skipby = parseInt(req.body.pagination);
    } else {
      var skipby = 0;
    }
    hart.matrimony_profile.find(Query).exec(function (err, doc1) {
      if (err) {
        console.log(err);
      } else {
        res.json(doc1)
      }
    });
  })
  app.post('/getsimilarprofiles', function (req, res) {
    if (req.session.Gender == "MALE") {
      var Gender = "FEMALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };

    } else {
      var Gender = "MALE";
      var Query = { "$and": [{ "Gender": Gender }, { "Status": "1" }] };
    }
    if (req.body.pagination != "") {
      var skipby = parseInt(req.body.pagination);
    } else {
      var skipby = 0;
    }
    hart.matrimony_profile.find(Query).sort({ _id: -1 }).skip(skipby).limit(10).exec(function (err, doc1) {
      if (err) {
        console.log(err);
      } else {
        res.json(doc1)
      }
    });
  })


  app.post('/SpecialSearch', function (req, res) {
    if (req.session.Gender == "MALE") {
      var gender = "FEMALE";
    } else {
      var gender = "MALE";
    }
    if (req.body.memberid != "") {
      console.log(req.body)
      hart.matrimony_profile.find({ "$and": [{ "Status": "1" }, { "matrimony_id": req.body.memberid }] }).exec(function (err, doc) {
        if (!err) {
          res.json(doc);
        } else {
          res.json("0");
        }
      });
      return false;
    } else {
      console.log("step1")
      if (req.body.pagination != "") {
        var skipby = parseInt(req.body.pagination) * 25;
      } else {
        var skipby = 0;
      }
      console.log(req.body)
      if (req.body.caste != "All" || req.body.religion != "All") {
        console.log("step123")
        console.log(gender);

        hart.matrimony_profile.find({ "$and": [{ "Gender": gender }, { "Status": "1" }, { "Age": { $gt: req.body.agefrom } }, { "Age": { $lt: req.body.ageto } },{"$or": [{ "Caste": req.body.caste }, { "Religion": req.body.religion }]}, { "MaritialStatus": req.body.maritalstatus }] }).skip(skipby).limit(20).exec(function (err, doc) {
          if (!err) {
            res.json(doc);
          } else {
            res.json("0");
          }
        })
      } else {
        hart.matrimony_profile.find({ "$and": [{ "Gender": gender }, { "Status": "1" }, { "Age": { $gt: req.body.agefrom } }, { "Age": { $lt: req.body.ageto } }, { "$or": [{ "Religion": req.body.religion }, { "MaritialStatus": req.body.maritalstatus }, { "Caste": req.body.caste }] }] }).skip(skipby).limit(20).exec(function (err, doc) {
          if (!err) {
            res.json(doc);
          } else {
            res.json("0");
          }
        })
      }
    }

  });



  app.post('/getprofileslength_search', function (req, res) {
    if (req.session.gender == "MALE") {
      var gender = "FEMALE";
    } else {
      var gender = "MALE";
    }
    if (req.body.caste != "All" || req.body.religion != "All") {
      hart.matrimony_profile.find({ "$and": [{ "Gender": gender }, { "Status": "1" }, { "Age": { $gt: req.body.agefrom } }, { "Age": { $lt: req.body.ageto } }, { "Caste": req.body.caste }, { "Religion": req.body.religion }, { "MaritialStatus": req.body.maritalstatus }] }).exec(function (err, doc) {
        if (!err) {
          res.json(doc);
        } else {
          res.json("0");
        }
      })
    } else {
      hart.matrimony_profile.find({ "$and": [{ "Gender": gender }, { "Status": "1" }, { "Age": { $gt: req.body.agefrom } }, { "Age": { $lt: req.body.ageto } }, { "$or": [{ "Religion": req.body.religion }, { "MaritialStatus": req.body.maritalstatus }, { "Caste": req.body.caste }] }] }).exec(function (err, doc) {
        if (!err) {
          res.json(doc);
        } else {
          res.json("0");
        }
      })
    }
  })

}
