//var CryptoJS = require("crypto-js");
//var secretkey = '0678056d96914a8583fb518caf42828a'; //Test
var secretkey = '';  //live

exports.getChecksumString = function(data) {
  console.log(data);
  var checksumstring = "";
  var checksumsequence = ["amount","bankid","buyerEmail","buyerFirstName","buyerLastName","buyerPhoneNumber","currency","merchantIdentifier","orderId","product1Description","product2Description","returnUrl"];
  for (var seq in checksumsequence) {
    for (var key in data) {
      if((key.toString()) === checksumsequence[seq]) {
        if(data[key].toString() !== "") {
          checksumstring += key+"="+data[key].toString()+"&";
        }
      }
    }
  }
  return checksumstring;
}

exports.getResponseChecksumString = function(data) {
  var checksumstring = "";
  var checksumsequence = ["amount","bank","bankid","cardId",
        "cardScheme","cardToken","cardhashid","doRedirect","orderId",
        "paymentMethod","paymentMode","responseCode","responseDescription",
        "productDescription","product1Description","product2Description",
        "product3Description","product4Description","pgTransId","pgTransTime"];

  for (var seq in checksumsequence) {
    for (var key in data) {
      if((key.toString()) === checksumsequence[seq]) {
        checksumstring += key+"="+data[key].toString()+"&";
      }
    }
  }
  return checksumstring;
}

exports.getCheckAndUpdateChecksumString = function(data) {
  var checksumstring = "";
  for (var key in data) {
    checksumstring += "'"+data[key]+"'";
  }
  return checksumstring;
}

exports.calculateChecksum = function(checksumstring) {
  //return CryptoJS.HmacSHA256(checksumstring,secretkey);
}
